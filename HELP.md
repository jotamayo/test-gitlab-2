# Getting Started

### How to use

##### Swagger2

You need put a token into field value with format

Bearer {{Token}}

For get token, please consider the following section:

* (https://confluence.mango.com/display/DEVOPS/mng-authentication)

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)

