package org.example.testgitlab2.utils

import org.example.testgitlab2.model.Example
import java.math.BigDecimal

class MockDataFunctions {
    companion object{
        fun getMockExamples() : List<Example>{
            return listOf(
                    Example(BigDecimal(1), "Example 1"),
                    Example(BigDecimal(2), "Example 2"),
                    Example( BigDecimal(3), "Example 3"),
                    Example( BigDecimal(4), "Example 4"),
                    Example( BigDecimal(5), "Example 5"),
                    Example( BigDecimal(6), "Example 6")
            )
        }
    }
}