package org.example.testgitlab2.utils

class MockDataVariables {
    companion object{
        val INVALID_EXAMPLE_ID = 7
        val VALID_EXAMPLE_ID = 1
        val ALL_EXAMPLES_RESPONSE_JSON = "[{\"id\":1,\"name\":\"Example 1\"},{\"id\":2,\"name\":\"Example 2\"},{\"id\":3,\"name\":\"Example 3\"},{\"id\":4,\"name\":\"Example 4\"},{\"id\":5,\"name\":\"Example 5\"},{\"id\":6,\"name\":\"Example 6\"}]"
    }
}