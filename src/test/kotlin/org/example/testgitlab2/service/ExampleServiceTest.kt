package org.example.testgitlab2.service

import org.example.testgitlab2.exception.NotFoundException
import org.example.testgitlab2.model.Example
import org.example.testgitlab2.repository.ExampleRepository
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import java.math.BigDecimal

@SpringBootTest
@RunWith(SpringRunner::class)
class ExampleServiceTest {

    var exampleList: MutableList<Example> =  arrayListOf()

    @MockBean
    lateinit var repository: ExampleRepository


    @Autowired
    lateinit var service: ExampleService

    @Before
    fun initialize() {

        exampleList.add( Example( BigDecimal(1), "Example 1"));
        exampleList.add( Example( BigDecimal(2), "Example 2"));
        exampleList.add( Example( BigDecimal(3), "Example 3"));
        exampleList.add( Example( BigDecimal(4), "Example 4"));
        exampleList.add( Example( BigDecimal(5), "Example 5"));
        exampleList.add( Example( BigDecimal(6), "Example 6"));

        given(repository.getAll()).willReturn(exampleList)
    }

    @Test
    fun `is ok when we search with a valid ID`() {
        Assert.assertEquals( Example( BigDecimal(2), "Example 2").id, service.getAll()[1].id);
    }

    @Test(expected = NotFoundException::class)
    fun `launch NotFoundException when we search with an invalid id`() {
        service.getExample(BigDecimal(7));
    }

    @Test
    fun `get all return 6 valid objects`(){
        Assert.assertEquals(6, service.getAll().size.toLong());
    }

    @Test
    fun `return success on insert`(){
        var example : Example  = Example( BigDecimal(7), "Example 7");
        exampleList.add(example);
        Assert.assertEquals(example, service.add(example));
        Assert.assertEquals(7, service.getAll().size.toLong());
    }
}