package org.example.testgitlab2.configuration

import org.joda.time.LocalDate

import org.springframework.context.annotation.Bean
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.*
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import org.springframework.context.annotation.Configuration
import springfox.documentation.spi.service.contexts.SecurityContext
import springfox.documentation.swagger2.annotations.EnableSwagger2
import org.springframework.context.annotation.Profile
import java.util.*

@Configuration
@EnableSwagger2
@Profile("!prod")
class SwaggerConfiguration {

    private val AUTHORIZATION = "Authorization"

    @Bean
    fun api(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("org.example.test-gitlab-2"))
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/")
                .directModelSubstitute(LocalDate::class.java, String::class.java)
                .useDefaultResponseMessages(false)
                .securitySchemes(Collections.singletonList(apiKey()))
                .securityContexts(Collections.singletonList(securityContext()))
                .apiInfo(apiInfo())
    }

    fun  apiInfo() : ApiInfo = ApiInfoBuilder()
            .title("REST API")
            .description("REST API for ")
            .version("1.0.0")
            .build()

    fun apiKey(): ApiKey {
        return ApiKey(AUTHORIZATION, AUTHORIZATION, "header")
    }

    fun securityContext(): SecurityContext {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.regex("/test-gitlab-2.*"))
                .build()
    }

    fun defaultAuth(): List<SecurityReference> {
        val authorizationScope = AuthorizationScope("global", "accessEverything")
        val authorizationScopes = arrayOfNulls<AuthorizationScope>(1)
        authorizationScopes[0] = authorizationScope
        return Collections.singletonList(SecurityReference(AUTHORIZATION, authorizationScopes))
    }
}