package org.example.testgitlab2.service

import org.example.testgitlab2.exception.NotFoundException
import org.example.testgitlab2.model.Example
import org.example.testgitlab2.repository.ExampleRepository
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class ExampleService( val exampleRepository: ExampleRepository) {

    fun getExample(id: BigDecimal): Example {
        val op = exampleRepository.getAll()
                .stream()
                .filter { example -> example.id == id }
                .findFirst()
        return if (op.isPresent) {
            op.get()
        } else {
            throw NotFoundException("Example not found.")
        }
    }

    fun getAll(): List<Example> {
        return exampleRepository.getAll()
    }

    fun add(example: Example): Example {
        exampleRepository.add(example)
        return example
    }
}