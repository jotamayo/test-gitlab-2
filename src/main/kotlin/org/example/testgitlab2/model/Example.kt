package  org.example.testgitlab2.model

import java.math.BigDecimal

data class Example (var id: BigDecimal, var name: String)