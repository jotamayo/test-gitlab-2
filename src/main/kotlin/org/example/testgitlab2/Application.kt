package org.example.testgitlab2

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication(scanBasePackages = ["org.example.testgitlab2"])
class Application

fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}