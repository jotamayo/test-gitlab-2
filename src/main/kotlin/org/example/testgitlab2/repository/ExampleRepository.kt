package  org.example.testgitlab2.repository

import org.example.testgitlab2.model.Example
import org.springframework.stereotype.Repository

@Repository
class ExampleRepository {

    var examples: MutableList<Example> =  mutableListOf()

    fun getAll(): List<Example> {
        return examples
    }

    fun add(example: Example): List<Example> {
        examples.add(example)
        return examples.toList()
    }
}