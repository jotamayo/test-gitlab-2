package  org.example.testgitlab2.exception

import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import org.springframework.http.HttpStatus
import javax.servlet.http.HttpServletRequest
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import java.time.LocalDateTime

@ControllerAdvice
class AppResponseEntityExceptionHandler: ResponseEntityExceptionHandler() {

    @ResponseBody
    @ExceptionHandler(UnsupportedOperationException::class)
    @ResponseStatus(HttpStatus.NOT_IMPLEMENTED)
    fun handleNotImplementedException(exception: RuntimeException, httpServletRequest: HttpServletRequest): ResponseEntity<ApiError> {
        return ResponseEntity(createError(exception, httpServletRequest, HttpStatus.NOT_IMPLEMENTED), HttpStatus.NOT_IMPLEMENTED)
    }

    @ResponseBody
    @ExceptionHandler(NotFoundException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun handleNotFoundException(exception: RuntimeException, httpServletRequest: HttpServletRequest): ResponseEntity<ApiError> {
        return ResponseEntity(createError(exception, httpServletRequest, HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND)
    }


    /**
     * Create an ApiError
     *
     * @param exception
     * @param httpServletRequest
     * @param errorCode
     * @return
     */
    private fun createError(exception: RuntimeException, httpServletRequest: HttpServletRequest, errorCode: HttpStatus): ApiError {
        return ApiError()
                .setTimestamp(LocalDateTime.now())
                .setStatus(errorCode.value())
                .setError(errorCode.reasonPhrase)
                .setException(exception.javaClass.canonicalName)
                .setMessage(exception.localizedMessage)
                .setPath(httpServletRequest.requestURI)
    }
}