package  org.example.testgitlab2.exception

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDateTime

class ApiError {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private var timestamp: LocalDateTime? = null
    private var status: Int = 0
    private var error: String? = null
    private var exception: String? = null
    private var message: String? = null
    private var path: String? = null

    /**
     * Gets timestamp.
     *
     * @return the timestamp
     */
    fun getTimestamp(): LocalDateTime? {
        return timestamp
    }

    /**
     * Sets timestamp.
     *
     * @param timestamp the timestamp
     * @return the timestamp
     */
    fun setTimestamp(timestamp: LocalDateTime): ApiError {
        this.timestamp = timestamp
        return this
    }

    /**
     * Gets status.
     *
     * @return the status
     */
    fun getStatus(): Int {
        return status
    }

    /**
     * Sets status.
     *
     * @param status the status
     * @return the status
     */
    fun setStatus(status: Int): ApiError {
        this.status = status
        return this
    }

    /**
     * Gets error.
     *
     * @return the error
     */
    fun getError(): String? {
        return error
    }

    /**
     * Sets error.
     *
     * @param error the error
     * @return the error
     */
    fun setError(error: String): ApiError {
        this.error = error
        return this
    }

    /**
     * Gets exception.
     *
     * @return the exception
     */
    fun getException(): String? {
        return exception
    }

    /**
     * Sets exception.
     *
     * @param exception the exception
     * @return the exception
     */
    fun setException(exception: String): ApiError {
        this.exception = exception
        return this
    }

    /**
     * Gets message.
     *
     * @return the message
     */
    fun getMessage(): String? {
        return message
    }

    /**
     * Sets message.
     *
     * @param message the message
     * @return the message
     */
    fun setMessage(message: String): ApiError {
        this.message = message
        return this
    }

    /**
     * Gets path.
     *
     * @return the path
     */
    fun getPath(): String? {
        return path
    }

    /**
     * Sets path.
     *
     * @param path the path
     * @return the path
     */
    fun setPath(path: String): ApiError {
        this.path = path
        return this
    }
}