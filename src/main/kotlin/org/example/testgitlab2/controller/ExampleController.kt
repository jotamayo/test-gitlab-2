package  org.example.testgitlab2.controller

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

import org.example.testgitlab2.exception.ApiError
import org.example.testgitlab2.model.Example
import org.example.testgitlab2.service.ExampleService

import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import io.swagger.annotations.Api
import java.math.BigDecimal

@RestController
@RequestMapping("test-gitlab-2")
@Api(tags = ["examples"])
class ExampleController(private val exampleService: ExampleService) {

    @ApiOperation(value = "Get an Example", response = Example::class)
    @ApiResponses(value = [ApiResponse(code = 403, message = "Forbidden", response = ApiError::class), ApiResponse(code = 404, message = "Not Found", response = ApiError::class), ApiResponse(code = 500, message = "Internal Server Error", response = ApiError::class)])
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{id}")
    fun getExample(@ApiParam(value = "The ID of the Example", required = true) @PathVariable("id") id: BigDecimal): Example {
        return exampleService.getExample(id)
    }

    @ApiOperation(value = "Get all examples", response = Example::class, responseContainer = "List")
    @ApiResponses(value = [ApiResponse(code = 403, message = "Forbidden", response = ApiError::class), ApiResponse(code = 404, message = "Not Found", response = ApiError::class), ApiResponse(code = 500, message = "Internal Server Error", response = ApiError::class)])
    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    fun getAll(): List<Example> {
        return exampleService.getAll()
    }

    @ApiOperation(value = "Create a new Example", response = Example::class)
    @ApiResponses(value = [ApiResponse(code = 201, message = "Created"), ApiResponse(code = 400, message = "Bad Request", response = ApiError::class), ApiResponse(code = 401, message = "Unauthorized", response = ApiError::class), ApiResponse(code = 500, message = "Internal Server Error", response = ApiError::class)])
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    fun createExample(@RequestBody example: Example): Example {
        exampleService.add(example)
        return example
    }

}