FROM adoptopenjdk/openjdk11:alpine-jre
COPY ${artifactId}/target/*.jar app.jar
RUN apk add --no-cache tzdata
ENV TZ Europe/Madrid
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=${ENV}","-jar","/app.jar"]